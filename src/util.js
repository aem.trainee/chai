function showMessage(messageInput){
	if(messageInput ==''){
		return 'InputError: Message is empty'
	}

	if(messageInput == undefined){
		return 'InputError: Message is undefined'
	}

	if(typeof(messageInput) !== 'string'){
		return 'InputError: Message is not a string'
	}

	return messageInput;
}

function factorial(n){
  // if (n === 0 || n === 1)
  //   return 1;
  // 	for (var i = n - 1; i >= 1; i--) {
  //   n *= i;
  // }

	if(n == null){
		return 'InputError: n is null'
	}

	if(n == undefined){
		return 'InputError: n is undefined'
	}

	if(typeof(n) !== 'number'){
		return 'InputError: n is not an integer'
	}

	if(n === 0){
		return 'InputError: n is not zero'
	}

  return n;
}

module.exports = {
	showMessage, factorial
}
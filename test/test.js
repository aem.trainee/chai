const { showMessage } = require('../src/util.js');
const { factorial } = require('../src/util.js');
const { expect, assert } = require('chai');

//describe('description', callback fn)
//it()

describe('test_showMessage',() => {
	it('message_Not_Empty', () => {
		const message = '';
		assert.isNotEmpty(showMessage(message))
	});

	it('message_Not_undefined', () => {
		const message = undefined;
		assert.isDefined(showMessage(message))
	});

	// it('message_isNot_string', () => {
	// 	const message = 'string';
	// 	assert.isNotString(showMessage(message))
	// });

	it('message_type_string', () => {
		const message = 5;
		assert.typeOf(showMessage(message), 'string');
	});

});


//activity - factorial
describe('test_factorial',() => {

	it('input_Not_null', () => {
		const n = null;
		assert.isNotNull(factorial(n))
	});

	it('input_Not_undefined', () => {
		const n = undefined;
		assert.isDefined(factorial(n))
	});

	it('input_isNot_number', () => {
		const n = "hi";
		assert.isNotNumber(factorial(n));
	});

	it('input_isNot_zero', () => {
		const n = 1;
		assert.notEqual(factorial(n),0);
	});

});